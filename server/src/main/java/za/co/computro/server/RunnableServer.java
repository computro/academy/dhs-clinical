package za.co.computro.server;

import java.io.IOException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.co.computro.config.ApplicationConfig;
import za.co.computro.util.Logger;

/**
 * Entry point of the DHS Clinical System server (Starts the server)
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public class RunnableServer {

    private static ApplicationContext ctx;
    private static Logger logger;

    /**
     * Starts the server - server's main method
     *
     * @param args - args[0] may contain the port number
     */
    public static void main(String[] args) {
        // Create an instance of applicationContext to access your beans
        ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        logger = ctx.getBean(Logger.class);
        /*
         Set the port number using the value from program arguments if one
         is set, otherwise set the default to 9990
         */
        int portNumber = args.length > 0 ? Integer.parseInt(args[0]) : 9991;

        try {
            // Retreive an instance of the DHS server
            RemoteServer remoteServer = ctx.getBean(RemoteServer.class);
            
            // Start the server - provide port to listen at
            remoteServer.start(portNumber);

        } catch (IOException e) {
            logger.logError(e.getMessage());
        }
    }
}
