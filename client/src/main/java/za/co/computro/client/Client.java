package za.co.computro.client;

/**
 * Basic DHS client interface
 * 
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public interface Client {
    /**
     * Define the run behavior of a DHS Clinical Client application
     */
    public void run();
}
