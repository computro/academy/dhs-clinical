package za.co.computro.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.co.computro.config.ApplicationConfig;
import za.co.computro.util.Logger;

/**
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public class RemoteClientImpl implements RemoteClient {

    private Socket socketClient;
    private String fileName;
    private String host;
    private int port;
    
    private static final ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
    private static Logger logger;

    static {
        try {
            logger = ctx.getBean(Logger.class);
        } catch (Exception e) {
            System.err.println("Logger not available: " + e.getMessage());
        }
    }

    /**
     * Read the expected file contents or error message coming from the server
     * 
     * @throws IOException 
     */
    @Override
    public void readResponse() throws IOException {
        String userInput;
        try (BufferedReader stdIn = new BufferedReader(new InputStreamReader(socketClient.getInputStream()))) {
            logger.logMessage("Receiving response from the server (RESPONSE:\n");
            while ((userInput = stdIn.readLine()) != null) {
                logger.logMessage(userInput);
            }
            logger.logMessage("");
        }
    }

    /**
     * Send the provided fileName to the server through the open IO stream
     * 
     * @param filename
     * @throws IOException 
     */
    @Override
    public void sendRequest(String filename) throws IOException {
        //Send the message to the server
        BufferedWriter bw = new BufferedWriter(new PrintWriter(socketClient.getOutputStream(), true));
        bw.write(filename + "\n");
        bw.flush();
        logger.logMessage("Requested contents of " + filename + " on the server...");
    }

    /**
     * Attempts to connect to a remote server (Typically the DHS Clinical 
     * server) running on the given host name and port number
     * 
     * @param hostname
     * @param port
     * @throws UnknownHostException
     * @throws IOException 
     */
    @Override
    public void connect(String hostname, int port) throws UnknownHostException, IOException {
        logger.logMessage("\nAttempting to connect to " + hostname + ":" + port);
        socketClient = new Socket(hostname, port);
        logger.logMessage("Connection Established!\n");
    }

    /**
     * Close open IO connections
     */
    @Override
    public void closeConnection() {
        try {
            socketClient.close();
            logger.logMessage("Connection closed!");
        } catch (IOException ex) {
            logger.logError("Error while closing connection: " + ex.getMessage());
        }
    }

    /**
     * Runs the DHS client in remote mode
     * 
     * @see RemoteClient
     */
    @Override
    public void run() {
        try {
            //Print/save the System parameters and filename
            logger.logMessage("\nHost: " + host + " Port: " + port + " Filename: " + fileName + "\n");
            //trying to establish connection to the server
            connect(host, port);
            //if handshake successful, send the filename to the server
            sendRequest(fileName);
            //read and display the response from server
            readResponse();
            //Closes the socket connetion
            closeConnection();

        } catch (UnknownHostException e) {
            logger.logError("Unable to establish a connection, the host is unknown.");
        } catch (IOException e) {
            logger.logError("Unable to establish a connection, the server may be down: " + e.getMessage());
        }
    }

    /**
     * Setter for ApplicationConfig to be able to set the fileName
     * 
     * @param fileName 
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Setter for ApplicationConfig to be able to set the host name
     * 
     * @param host 
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Setter for ApplicationConfig to be able to set the port number
     * 
     * @param port 
     */
    public void setPort(int port) {
        this.port = port;
    }
}
