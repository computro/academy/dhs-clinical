package za.co.computro.util;

/**
 * Here you can implement your logger to log to a file, 
 * DB or even a combination of a few destinations.
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public abstract class Logger {

    /**
     * Writes a normal message to the standard output
     *
     * @param message
     */
    public abstract void logMessage(String message);

    /**
     * Logs an error to the standard error output
     *
     * @param errorMessage
     */
    public abstract void logError(String errorMessage);
}
