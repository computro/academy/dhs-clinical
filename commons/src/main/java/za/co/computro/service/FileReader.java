package za.co.computro.service;

/**
 * Generic file reader interface
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public interface FileReader {
    String readFile(String path) throws ReaderException;
}
