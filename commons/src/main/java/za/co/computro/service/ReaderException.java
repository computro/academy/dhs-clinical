package za.co.computro.service;

/**
 * Represents an exception that occurred in the FileReader
 * 
 * @see FileReader
 * 
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public class ReaderException extends Exception {

    public ReaderException(String message) {
        super(message);
    }

    public ReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
