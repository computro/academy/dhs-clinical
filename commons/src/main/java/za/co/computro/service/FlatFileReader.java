package za.co.computro.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Implementation of FileReader class
 * Reads human readable text from a text file and return
 * the contents as a string
 * 
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public class FlatFileReader implements FileReader {

    /**
     * Reads and returns contents of a flat file as a string 
     * (uses NIO to read file content)
     * 
     * @param path
     * @return file contents - as readable string
     * @throws ReaderException 
     */
    @Override
    public synchronized String readFile(String path) throws ReaderException {
        BufferedReader reader = null;
        try {
            Path file = Paths.get(path);
            
            //Set the charset to ASCII
            Charset charset = Charset.forName("US-ASCII");
            reader = Files.newBufferedReader(file, charset);
            
            StringBuilder content = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line).append("\n");
            }
            return content.toString();
        } catch (IOException ex) {
            throw new ReaderException("Error reading content: " + ex.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {}
        }
    }
}
