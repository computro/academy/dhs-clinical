package za.co.computro.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import za.co.computro.server.RemoteServer;
import za.co.computro.server.SocketServer;
import za.co.computro.util.Logger;

/**
 * Can replace config with xml for external control (e.g. to override 
 * initial configurations without recompiling the code)
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
@Configuration
@Import(FileReaderConfig.class)
public class ApplicationConfig {
    //Only one instance allowed in memory
    private SocketServer socketServer;
    
    /**
     * 
     * @return instance of SocketServer
     */
    @Bean
    public RemoteServer remoteServer() {
        return socketServer == null ? (socketServer = new SocketServer()) : socketServer;
    }
    
    /**
     * 
     * @return instance of logger
     */
    @Bean
    public Logger logger() {
        return new Logger() {

            @Override
            public void logMessage(String message) {
                System.out.println(message);
            }

            @Override
            public void logError(String errorMessage) {
                System.err.println(errorMessage);
            }
        };
    }
}
