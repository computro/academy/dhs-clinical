package za.co.computro.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.co.computro.config.ApplicationConfig;
import za.co.computro.service.FileReader;
import za.co.computro.service.ReaderException;
import za.co.computro.util.Logger;

/**
 * Implementation of a Remote Server for the DHS system
 * Defines a private thread class to handle DHS client requests
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public class SocketServer implements RemoteServer {

    // Create an instance of applicationContext to access your beans
    private static final ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
    private static Logger logger;

    private ServerSocket serverSocket;
    
    static {
        try {
            logger = ctx.getBean(Logger.class);
        } catch (Exception e) {
            System.err.println("Logger not available: " + e.getMessage());
        }
    }

    /**
     * Starts the socket server and waits for client requests
     *
     * @param port
     * @throws IOException
     */
    @Override
    public void start(int port) throws IOException {
        logger.logMessage("Starting the socket server at port:" + port);
        serverSocket = new ServerSocket(port);

        while (true) {
            //Waiting for client connections...
            logger.logMessage("Waiting for client connections...");
            Socket client = serverSocket.accept();
            logger.logMessage("Client connected...");

            //Handle client request in a thread and return to listen
            RequestHandler handler = new RequestHandler();
            handler.setClient(client);
            handler.start();
        }
    }

    /**
     * Gets the file path from the client socket
     *
     * @param client
     * @return path - a string
     * @throws IOException
     */
    private String retrieveClientMessage(Socket client) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));

        // Read the message from the client
        String message = br.readLine();
        logger.logMessage("Filename received! Filename: " + message);
        return message;
    }

    /**
     * A thread class to handle client requests
     */
    private class RequestHandler extends Thread {

        private Socket client;

        /**
         * Retrieves a filename from client request and serves the content of
         * the file as a string
         *
         * @param client - a client socket
         * @throws IOException
         */
        @Override
        public void run() {
            try (BufferedWriter writer = new BufferedWriter(new PrintWriter(
                    client.getOutputStream(), true))) {
                
                try {
                    logger.logMessage("Retrieving client message (the filename)...");
                    String filename = retrieveClientMessage(client);

                    FileReader reader = ctx.getBean(FileReader.class);
                    String contents = reader.readFile(filename);

                    //Return the contents of the file (response)
                    writer.write(contents + "\n");
                    writer.flush();
                } catch (ReaderException re) {
                    
                    logger.logError("Error while serving client: " 
                            + client.getLocalAddress() + " (" + re.getMessage() + ")");
                    
                    writer.write(re.getMessage() + "\n");
                    writer.flush();
                }
                
            } catch (IOException ioe) {
                logger.logError("An IO error occured while serving client: " 
                        + client.getLocalAddress() + " (" + ioe.getMessage() + ")");
            }
        }

        /**
         * Setter to allow the SocketServer class to be able to set the client
         * reference
         *
         * @param client
         */
        public void setClient(Socket client) {
            this.client = client;
        }
    }
}
