package za.co.computro.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import za.co.computro.client.Client;
import za.co.computro.client.RemoteClientImpl;
import za.co.computro.client.StandaloneClient;
import za.co.computro.util.Constant;
import za.co.computro.util.Logger;

/**
 * Can replace config with xml for external control (e.g. to override 
 * initial configurations without recompiling the code)
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
@Configuration
@Import(FileReaderConfig.class)
public class ApplicationConfig {

    /**
     * Respective of set System Properties, the method returns an instance of a
     * standalone client or remote / socket client.
     *
     * If the system properties (server.host and server.port) are set, then
     * setup an instance of an implementation of RemoteClient otherwise setup
     * the standalone client and return the instance
     *
     * @see Constant
     *
     * @return an instance of a DHS Clinical System client
     */
    @Bean
    public Client client() {
        String host = System.getProperty(Constant.HOST.toString());
        String port = System.getProperty(Constant.PORT.toString());
        String filename = System.getProperty(Constant.FILENAME.toString());

        if (host != null && port != null) {
            // All needed system properties (server.host and server.port) are set,
            // therefore setup an instance of an implementation of RemoteClient
            // and return the instance
            RemoteClientImpl client = new RemoteClientImpl();
            client.setFileName(filename);
            client.setHost(host);
            client.setPort(Integer.parseInt(port));
            return client;
        } else {
            // Not all needed system properties are set, therefore
            // setup the standalone client and return the instance
            StandaloneClient client = new StandaloneClient();
            client.setFileName(filename);
            return client;
        }
    }
    
    @Bean
    public Logger logger() {
        return new Logger() {

            @Override
            public void logMessage(String message) {
                System.out.println(message);
            }

            @Override
            public void logError(String errorMessage) {
                System.err.println(errorMessage);
            }
        };
    }
}
