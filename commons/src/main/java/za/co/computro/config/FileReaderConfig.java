package za.co.computro.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import za.co.computro.service.FileReader;
import za.co.computro.service.FlatFileReader;

/**
 * Can replace config with xml for external control (e.g. to override 
 * initial configurations without recompiling the code)
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
@Configuration
public class FileReaderConfig {

    /**
     * instance of FlatFileReader
     * @return 
     */
    @Bean
    public FileReader fileReader() {
        return new FlatFileReader();
    }
}
