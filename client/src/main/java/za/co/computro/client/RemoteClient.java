package za.co.computro.client;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * This interface defines the expected behavior of a remote client
 * connecting to the DHS Clinical Server
 * 
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public interface RemoteClient extends Client {

    /**
     * Opens an IO stream between this client and the server listening on the 
     * specified port of the host also provided.
     * 
     * @param hostname
     * @param port
     * @throws UnknownHostException
     * @throws IOException 
     */
    public void connect(String hostname, int port) throws UnknownHostException, IOException;
    
    /**
     * Send request to connected server throw the open IO stream
     * The request includes the fileName on the server machine of which
     * the contents will be feed back to the client
     * 
     * @param filename
     * @throws IOException 
     */
    public void sendRequest(String filename) throws IOException;
    
    /**
     * Read response from the connected server throw the open IO stream
     * 
     * @throws IOException 
     */
    public void readResponse() throws IOException;
    
    /**
     * closes all open IO streams
     */
    public void closeConnection();
}
