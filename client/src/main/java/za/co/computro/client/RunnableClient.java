package za.co.computro.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.co.computro.config.ApplicationConfig;

/**
 * Entry point for the client application
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public class RunnableClient {
    
    private static ApplicationContext ctx;
    
    /**
     * Starts the client application either in remote mode or local (in which
     * case the contents of a file on the local machine are retrieved)
     *
     * @param args
     */
    public static void main(String[] args) {
        // Create an instance of applicationContext to access your beans
        ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        Client client = ctx.getBean(Client.class);
        client.run();
    }
}
