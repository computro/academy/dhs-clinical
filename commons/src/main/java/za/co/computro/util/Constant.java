package za.co.computro.util;

/**
 * Holds constant values used to lookup expected system properties
 * Use -Dserver.host=SERVER_NAME -Dserver.port=PORT_NUMBER 
 * -Dpath.file=FILE_NAME JVM options to set the system properties
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public enum Constant {

    HOST("server.host"), PORT("server.port"), FILENAME("path.file");

    private Constant(String value) {
        this.value = value;
    }

    private final String value;

    @Override
    public String toString() {
        return value;
    }
}
