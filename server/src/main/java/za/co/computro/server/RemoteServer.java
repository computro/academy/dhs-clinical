package za.co.computro.server;

import java.io.IOException;

/**
 * A basic interface of a DHS remote server
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public interface RemoteServer {
    void start(int port) throws IOException;
}
