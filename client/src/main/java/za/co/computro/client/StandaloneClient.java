package za.co.computro.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import za.co.computro.config.ApplicationConfig;
import za.co.computro.service.FileReader;
import za.co.computro.service.ReaderException;
import za.co.computro.util.Logger;

/**
 * Implements a standalone version of the DHS Clinical System
 *
 * @author <a href="mailto:mogowanemt@computro.co.za">Theresho</a>
 */
public class StandaloneClient implements Client {

    private static final ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
    private static Logger logger;

    static {
        try {
            logger = ctx.getBean(Logger.class);
        } catch (Exception e) {
            System.err.println("Logger not available: " + e.getMessage());
        }
    }

    private String fileName;

    /**
     * Runs as a standalone client
     */
    @Override
    public void run() {
        try {
            logger.logMessage("\nRunning locally... Filename: " + fileName + "\n");
            FileReader reader = ctx.getBean(FileReader.class);
            String contents = reader.readFile(fileName);
            logger.logMessage(contents);
            logger.logMessage("");
        } catch (ReaderException re) {
            logger.logError("Error while reading local file contents: " + re.getMessage() + ")");
        }
    }

    /**
     * Setter for ApplicationConfig to be able to set the fileName
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
